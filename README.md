# grap-odoo-env

This is the environment used by GRAP IT Team to develop on Odoo.
There commands should work on a debian based OS.

## Before

you Should have set ``git`` correctly.
For that purpose, run
```shell
sudo apt-get install git
git config --global user.email "you@exemple.com"
git config --global user.name "Your Name"
```
## First time
```shell

# Get Code
git clone https://gitlab.com/grap-rhone-alpes/grap-odoo-env -b 12.0
cd grap-odoo-env

# Install required librairies (first time), create users, etc...
sudo ./install.sh

# Launch Odoo
sudo su odoo12 -c "./env/bin/python ./src/odoo/odoo-bin -c ./odoo.cfg"

# you can use odoo locally (http://localhost:8012) with the credentials admin/admin
```

## Update
```shell

# Update code
git pull origin 12.0

# Update Python librairies
./env/bin/python -m pip install -r ./src/odoo/requirements.txt
./env/bin/python -m pip install -r ./requirements.txt

# Rebuild code (In or out the virtualEnv)
gitaggregate -c repos.yml aggregate

# If you want only to rebuild one directory
gitaggregate -c repos.yml aggregate -d src/my-favorite-directory/

# Generate Config file (./odoo.cfg)
odoo-tools-grap generate
```

## Apply copier Template on repo

```
cd ./src/grap/xxx
git checkout 12.0
git pull grap 12.0
git checkout -b 12.0-copier-2023-03-10
cp ../grap-odoo-custom/.copier-answers.yml ./
subl .copier-answers.yml
```

Update the file

```
git add .copier-answers.yml
git commit -m "[REF] Add copier answer file" --no-verify
copier copy https://github.com/grap/oca-addons-repo-template-new.git ./
```

+ Execute.

* Check with git diff

```
git add .
git commit -m "[REF] Initialize Copier template" --no-verify
pre-commit run -a
```

* Check with git diff

```
git commit -am "[REF] Apply Changes"
git push legalsylvain  12.0-copier-2023-03-10 

```